# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login
from django.views import View

from reports.forms import LoginForm


@method_decorator(csrf_exempt, name='dispatch')
class LoginView(View):
    form_class = LoginForm
    initial = {'error_message': ''}
    template_name = ''

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, self.initial)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = form.save()
            if user is not None:
            	if user.is_active:
            		login(request, user)
            		return render(request, 'reports/index.html', self.initial)
            else:
            	self.initial['error_message'] = 'Email or Password do not match.'
        
        return render(request, self.template_name, self.initial)