# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import hashlib
import constants
from django.db import models
from django.contrib.auth.models import User



# Create your models here.
class TblFranchise(models.Model):
	"""
	model containing the details of all the franchises.
	"""
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	fname = models.CharField(max_length=20, null=False, blank=False)
	lname = models.CharField(max_length=20, null=True, blank=True)
	is_admin = models.BooleanField(default=False)
	status = models.IntegerField(default=1, help_text="Active:1, Inactive:0")
	date_created = models.DateTimeField(auto_now_add=True)
	date_updated = models.DateTimeField(auto_now_add=True)
	

	def save(self, *args, **kwargs):
		if not self.pk:
		    self.date_created = constants.FORMATTED_TIME()
		self.date_updated = constants.FORMATTED_TIME()
		super(TblFranchise, self).save(*args, **kwargs)


	class Meta:
		db_table = 'tbl_franchise'
		managed = True



class TblPatient(models.Model):
	"""
	model containing the patient details of every franchise.
	"""
	franchise = models.ForeignKey(TblFranchise, on_delete=models.CASCADE)
	fname = models.CharField(max_length=20, null=False, blank=False)
	lname = models.CharField(max_length=20, null=True, blank=True)
	email = models.CharField(max_length=50, null=False, blank=False)
	age = models.CharField(max_length=20, null=False, blank=False)
	gender = models.CharField(max_length=10, null=False, blank=False)
	date_created = models.DateTimeField(auto_now_add=True)
	date_updated = models.DateTimeField(auto_now_add=True)


	class Meta:
		db_table = 'tbl_patient'
		managed = True


	def save(self, *args, **kwargs):
		if not self.id:
		    self.date_created = constants.FORMATTED_TIME()
		self.date_updated = constants.FORMATTED_TIME()
		super(TblPatient, self).save(*args, **kwargs)



class TblTestCategory(models.Model):
	"""
	model containing the different test category.
	"""
	name = models.CharField(max_length=50, null=False, blank=False)
	date_created = models.DateTimeField(auto_now_add=True)
	date_updated = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'tbl_test_categories'
		managed = True

	def save(self, *args, **kwargs):
		if not self.id:
			self.date_created = constants.FORMATTED_TIME()
		self.date_updated = constants.FORMATTED_TIME()
		super(TblTestCategory, self).save(*args, **kwargs)



class TblTest(models.Model):
	"""
	model containing the details of the test done in the admin pathology.
	"""
	category = models.ForeignKey(TblTestCategory, on_delete=models.CASCADE)
	name = models.CharField(max_length=50, null=False, blank=False)
	age_range = models.CharField(max_length=10, null=False, blank=False)
	price = models.IntegerField(default=100, null=False, blank=False)
	ideal_range = models.CharField(max_length=50, null=False, blank=False)
	date_created = models.DateTimeField(auto_now_add=True)
	date_updated = models.DateTimeField(auto_now_add=True)


	class Meta:
		db_table = 'tbl_test'
		managed = True


	def save(self, *args, **kwargs):
		if not self.id:
			self.date_created = constants.FORMATTED_TIME()
		self.date_updated = constants.FORMATTED_TIME()
		super(TblTest, self).save(*args, **kwargs)



class TblPatientTest(models.Model):
	"""
	model containing the details of the test taken by the patient.
	"""	
	test = models.ForeignKey(TblTest, on_delete=models.CASCADE)
	patient = models.ForeignKey(TblPatient, on_delete=models.CASCADE)
	test_no = models.IntegerField(default=1, null=False, blank=False)
	date_created = models.DateTimeField(auto_now_add=True)
	date_updated = models.DateTimeField(auto_now_add=True)


	class Meta:
		db_table = 'tbl_patient_test'
		managed = True


	def save(self, *args, **kwargs):
		if not self.id:
			self.date_created = constants.FORMATTED_TIME()
		self.date_updated = constants.FORMATTED_TIME()
		super(TblPatientTest, self).save(*args, **kwargs)



class TblPatientReport(models.Model):
	"""
	model containing the report of the patients.
	"""
	franchise = models.ForeignKey(TblFranchise, on_delete=models.CASCADE)
	patient_test = models.ForeignKey(TblPatientTest, on_delete=models.CASCADE)
	report_url = models.CharField(max_length=200, null=False, blank=False)
	test_no = models.IntegerField(default=1, null=False, blank=False)
	date_created = models.DateTimeField(auto_now_add=True)
	date_updated = models.DateTimeField(auto_now_add=True)


	class Meta:
		db_table = 'tbl_patient_report'
		managed = True


	def save(self, *args, **kwargs):
		if not self.id:
			self.date_created = constants.FORMATTED_TIME()
		self.date_updated = constants.FORMATTED_TIME()
		super(TblPatientTest, self).save(*args, **kwargs)


