import hashlib
from hashlib import md5
from django import forms
from django.contrib.auth import authenticate, login
from .models import TblFranchise



class LoginForm(forms.Form):
	username = forms.CharField(max_length=50, required=True)
	password = forms.CharField(max_length=50, required=True)


	def save(self):
		username = self.cleaned_data['username']
		password = self.cleaned_data['password']

		check_user = authenticate(username=username, password=password)
		return check_user


